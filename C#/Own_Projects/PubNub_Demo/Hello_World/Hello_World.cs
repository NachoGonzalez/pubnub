﻿using System;
using System.Collections.Generic;
using System.Threading;
using PubNubMessaging.Core;

namespace Hello_World
{
    public class Hello_World
    {
        static Pubnub pubnub;
        static public void Main()
        {
            Console.Clear();
            string publishKey = "pub-c-c91e821f-18b8-4598-905f-a6748432f27e";
            string subscribeKey = "sub-c-cfdde3b8-cdbe-11e6-b2ab-0619f8945a4f";
            string secretKey = "sec-c-ZDY5M2UwOTYtZGEzMy00MDg2LTk1OGEtZDczNjRlMjc4ZjRl";
            string authKey = "abcd";
            string cipherKey ="";
            string channel = "testing";
            int lectura = 0;
            string pre = "";

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.WriteLine("Init...");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("PublishKey: {0}", publishKey);
            Console.WriteLine("SubscribeKey: {0}", subscribeKey);
            Console.WriteLine("SecretKey: {0}", secretKey);
            Console.WriteLine("AuthKey: {0}", authKey);
            Console.WriteLine("CipherKey: {0}", cipherKey);
            Console.WriteLine("Channel: {0}", channel);
            Console.ResetColor();

            pubnub = new Pubnub(
                publishKey,
                subscribeKey
                );

            pubnub.AuthenticationKey = authKey;
            Console.WriteLine("-> 0.Help");
            Console.WriteLine("-> 1.Subscribe");
            Console.WriteLine("-> 2.Publish");
            Console.WriteLine("-> 3.DetailedHistory");
            Console.WriteLine("-> 4.GrantAccess");
            Console.WriteLine("-> 5.GrantPresenceAccess");
            Console.WriteLine("-> 6.ChangeChannel");
            Console.WriteLine("-> 7.Unsubscribe");
            Console.WriteLine("-> -1.Exit");

            while (lectura != -1)
            {

                pre = Console.ReadLine();
                if (string.IsNullOrEmpty(pre))
                {
                    lectura = 0;
                }
                else
                {
                    lectura = Int32.Parse(pre);
                }
                if (lectura == 0)
                {
                    Console.WriteLine("-> 0.Help");
                    Console.WriteLine("-> 1.Subscribe");
                    Console.WriteLine("-> 2.Publish");
                    Console.WriteLine("-> 3.DetailedHistory");
                    Console.WriteLine("-> 4.GrantAccess");
                    Console.WriteLine("-> 5.GrantPresenceAccess");
                    Console.WriteLine("-> 6.ChangeChannel");
                    Console.WriteLine("-> 7.Unsubscribe");
                    Console.WriteLine("-> -1.Exit");
                }
                else if(lectura == 1)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Subscribing...");
                    Console.ResetColor();

                    pubnub.Subscribe<string>(
                        channel,
                        DisplaySubscribeReturnMessage,
                        DisplaySubscribeConnectStatusMessage,
                        DisplayErrorMessage
                    );

                }
                else if (lectura == 2)
                {
                    string mensaje = "Text: Hi";
                    Console.WriteLine("¿Que quiere enviar?");
                    mensaje = Console.ReadLine();
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Publish...");
                    Console.ResetColor();

                    pubnub.Publish<string>(
                        channel,
                        mensaje,
                        DisplayReturnMessage,
                        DisplayErrorMessage
                    );
                }
                else if (lectura == 3)
                {
                    int history = 1;
                    Console.WriteLine("¿Cuantos registros quieres ver?");
                    history = Int32.Parse(Console.ReadLine());
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("DetailedHstory...");
                    Console.ResetColor();

                    pubnub.DetailedHistory<string>(
                        channel,
                        history, true,
                        DisplayReturnMessage,
                        DisplayErrorMessage);
                }
                else if (lectura == 4)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("GrantAcces...");
                    Console.ResetColor();

                    pubnub.GrantAccess<string>(
                        channel,
                        subscribeKey,
                        true,
                        true,
                        5,
                        DisplayReturnMessage,
                        DisplayErrorMessage);
                }
                else if (lectura == 5)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("GrantPrecenseAccess...");
                    Console.ResetColor();

                    pubnub.GrantPresenceAccess<string>(
                        channel,
                        subscribeKey,
                        true,
                        true,
                        5,
                        DisplayReturnMessage,
                        DisplayErrorMessage);
                }
                else if (lectura == 6)
                {
                    string newchannel = "my_channel";
                    Console.WriteLine("Inserte el nuevo canal");
                    newchannel = Console.ReadLine();
                    channel = newchannel;

                }
                else if (lectura == 7)
                {
                    pubnub.Unsubscribe<string>(
                            channel,
                            DisplayReturnMessage,
                            DisplaySubscribeConnectStatusMessage,
                            DisplaySubscribeDisconnectStatusMessage,
                            DisplayErrorMessage
                        );

                }
                else if (lectura == -1)
                {
                    Console.WriteLine("Bye bye!");
                }
                else
                {
                    Console.WriteLine("Comando no reconocido");
                }

                Thread.Sleep(1000);
                Console.WriteLine();
            }
            
        }

        static void DisplayErrorMessage(PubnubClientError result)
        {
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("ERROR MESSAGE");
            Console.ResetColor();
            Console.WriteLine(result.Message);
            Console.WriteLine(result.Description);
            Console.WriteLine();
        }

        static void DisplaySubscribeConnectStatusMessage(string result)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("SUBSCRIBE CONNECT CALLBACK");
            Console.ResetColor();
            Console.WriteLine(result);
        }

        static void DisplaySubscribeDisconnectStatusMessage(string result)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("SUBSCRIBE DISCONNECT CALLBACK");
            Console.ResetColor();
            Console.WriteLine(result);
        }

        static void DisplaySubscribeReturnMessage(string result)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("SUBSCRIBE REGULAR CALLBACK:");
            Console.ResetColor();
            Console.WriteLine(result);
            if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(result.Trim()))
            {
                List<object> deserializedMessage = pubnub.JsonPluggableLibrary.DeserializeToListOfObject(result);
                if (deserializedMessage != null && deserializedMessage.Count > 0)
                {
                    object subscribedObject = (object)deserializedMessage[0];
                    if (subscribedObject != null)
                    {
                        //IF CUSTOM OBJECT IS EXCEPTED, YOU CAN CAST THIS OBJECT TO YOUR CUSTOM CLASS TYPE
                        string resultActualMessage = pubnub.JsonPluggableLibrary.SerializeToJsonString(subscribedObject);
                    }
                }
            }
        }
        static void DisplayReturnMessage(string result)
        {
            Console.WriteLine(result);
            if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(result.Trim()))
            {
                List<object> deserializedMessage = pubnub.JsonPluggableLibrary.DeserializeToListOfObject(result);
                if (deserializedMessage != null && deserializedMessage.Count > 0)
                {
                    object[] message = pubnub.JsonPluggableLibrary.ConvertToObjectArray(deserializedMessage[0]);
                    if (message != null)
                    {
                        if (message.Length >= 0)
                        {
                            foreach (object item in message)
                            {
                                Console.WriteLine(item);
                                //IF CUSTOM OBJECT IS EXCEPTED, YOU CAN CAST THIS OBJECT TO YOUR CUSTOM CLASS TYPE
                            }
                        }
                    }
                }
            }
        }

    }
}
